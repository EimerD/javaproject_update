package com.JavaProject1.rest;

import com.JavaProject1.entities.Payment;
import com.JavaProject1.services.PaymentBs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collection;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/payments")
public class PaymentController {

    @Autowired
    PaymentBs service;

    Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @RequestMapping(value="/status",method=RequestMethod.GET)
    public String getStatus()
    {
        try{
            Thread.sleep(5000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        return "Rest Api is running";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
        public ResponseEntity<Object> save(@RequestBody Payment payment)
        {

            service.save(payment);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(payment.getId())
                    .toUri();
            logger.trace("Save payment accessed");
            return ResponseEntity.created(location).build();
        }

       @RequestMapping(value= "/all", method = RequestMethod.GET)
       public Collection<Payment> getAll()
       {return service.all();
       }

//    @RequestMapping(value= "/find/{paymentType}", method = RequestMethod.GET)
//        public ResponseEntity<Payment> getPaymentByPaymentTypeHandling404(
//            @PathVariable("paymentType") String paymentType)
//        {
//
//        Collection<Payment> payment = service.findByType(paymentType);
//        if (payment ==null){
//            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
//        }
//        else{
//            return new ResponseEntity<Payment>(payment,HttpStatus.OK);
//        }
//    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
        public Long rowCount ()
        {
            logger.trace("Row count accessed");
            return service.rowCount();

        }

    @RequestMapping(value= "/find/{id}", method = RequestMethod.GET)
        public ResponseEntity<Payment> getPaymentByIdHandling404(@PathVariable("id") int id){
            Payment payment = service.find(id);
            if (payment ==null){
                logger.trace("Find ID accessed sucessfully");
                return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
            }
            else{
                logger.trace("Find Id failed ");
                return new ResponseEntity<Payment>(payment,HttpStatus.OK);
            }
        }


}
