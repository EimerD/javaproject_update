package com.JavaProject1.services;

import com.JavaProject1.dao.IPaymentDao;
import com.JavaProject1.dao.PaymentDao;
import com.JavaProject1.entities.Payment;
import com.JavaProject1.exception.PaymentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class PaymentBs
{
    @Autowired
    private PaymentDao dao;


    public Payment find(int id) {
        if (id < 0) {
            throw new PaymentException("Requires a valid Id > 0");
        }
        else{
            return dao.findById(id);

        }
    }


    public void save(Payment payment)
    {
        dao.save(payment);
    }


    public Collection<Payment> findByType(String paymentType) {
        if (paymentType == null)
        {
            throw new PaymentException("Requires a payment type");
        }
        else
            {
            return dao.findByType(paymentType);

        }
    }

    public Long rowCount() {
        return dao.rowCount();
    }

    public Collection<Payment> all() {
        return dao.getAll();
    }
}
