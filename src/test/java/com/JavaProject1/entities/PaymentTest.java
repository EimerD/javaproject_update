package com.JavaProject1.entities;

import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import static org.springframework.test.util.AssertionErrors.assertEquals;
public class PaymentTest
{
@Test
public void testPayment1()
    {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();
    Payment paycash= new Payment(1,date,"Cash", 20.00, 123);
    assertEquals("payment amount",20.00, paycash.getAmount());
    System.out.println(paycash);
    }

    @Test
    public void testPayment2()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment paycard= new Payment(2,date,"Card", 1000.00, 145);
        assertEquals("payment type","Card", paycard.getPaymentType());
        System.out.println(paycard);
    }
}
