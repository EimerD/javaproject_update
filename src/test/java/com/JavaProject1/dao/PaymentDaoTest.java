package com.JavaProject1.dao;

import com.JavaProject1.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@SpringBootTest
public class PaymentDaoTest

{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    IPaymentDao dao;

    @Test
    public void testSavePayment()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Payment payment = new Payment(3,date, "Loan", 200, 145);
        dao.save(payment);
        payment = dao.findById(3);
        Assert.assertNotNull( "Payment not found", payment);

    }
    @Test
    public void testSaveCardPayment()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Payment payment = new Payment(2,date, "Card", 100, 167);
        dao.save(payment);
        payment = dao.findById(2);
        Assert.assertNotNull( "Payment not found", payment);

    }
    @Test
    public void testSaveCashPayment()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Payment payment = new Payment(1,date, "Cash", 20, 123);
        dao.save(payment);
        payment = dao.findById(1);
        Assert.assertNotNull( "Payment not found", payment);

    }

    @Test
    public void listAllPayments()
    {

        List<Payment> pay = Collections.unmodifiableList(mongoTemplate.findAll(Payment.class));
        pay.forEach(payment -> System.out.println(payment.getPaymentType()));
        assertEquals(5,pay.size() ); System.out.println(pay.size());
    }
    @Test
    public void testId(){
        Payment payment = dao.findById(1);
        assertNotNull(payment);
    }


}
