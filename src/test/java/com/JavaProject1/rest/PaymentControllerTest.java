package com.JavaProject1.rest;

import com.JavaProject1.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerTest
{

    @LocalServerPort
    int randomServerPort;
    @Test
    public void testStatus() throws URISyntaxException{
        RestTemplate restTemplate = new RestTemplate();
        final  String baseUrl = "http://localhost:"+randomServerPort+"/payments/status";
        URI uri = new URI(baseUrl);

        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
        Assert.assertEquals(200,result.getStatusCodeValue());
        Assert.assertEquals(true, result.getBody().contains("Rest Api is running"));
    }

    //   @Test
    public void testAddPayment() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        final  String baseUrl = "http://localhost:"+randomServerPort+"/payments";
        URI uri = new URI(baseUrl);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Payment payment = new Payment(6, date, "mortgage", 600,189 );



        ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

        Assert.assertEquals(200,result.getStatusCodeValue());
        Assert.assertEquals(true, result.getBody().contains("paymentList"));
    }
}
