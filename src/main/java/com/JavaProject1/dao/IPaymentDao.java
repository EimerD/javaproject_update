package com.JavaProject1.dao;

import com.JavaProject1.entities.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;
import java.util.List;

public interface IPaymentDao  {

    Payment findById(int id);
    void save(Payment payment);
    Collection<Payment> findByType(String paymentType);
    Long rowCount();
    Collection<Payment> getAll();

}
