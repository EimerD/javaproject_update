package com.JavaProject1.services;

import com.JavaProject1.dao.PaymentDao;
import com.JavaProject1.entities.Payment;
import com.JavaProject1.exception.PaymentException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

@SpringBootTest
public class PaymentBsTest
{


    @Autowired
    PaymentBs service;
    //
    @Autowired
    PaymentDao dao;

    @Test
    public void testPaymentServices()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        service.save(new Payment(6,date, "Credit Card", 92.00, 144) );
        Payment payment = service.find(6);

        assertNotNull(payment);

    }

    @Test
   public void testFindById(){
        Payment payment = service.find(1);
        assertNotNull(payment);
    }

    @Test
    public void testFindByIdGeneratesException()
    {
        Exception exception = assertThrows(PaymentException.class, () -> {service.find(-1);});
        String expectedMessage = "Requires a valid Id > 0";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
   @Test
    public void testPaymentTypeException()
    {
        Exception exception = assertThrows(PaymentException.class, () -> {service.findByType(null);});
        String expectedMessage = "Requires a payment type";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


}
