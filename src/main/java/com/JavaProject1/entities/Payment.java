package com.JavaProject1.entities;

import com.JavaProject1.exception.PaymentException;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
@Document
public class Payment {

    private int id;
    private Date paymentDate;
    private String paymentType;
    private double amount;
    private int customerId;

    public Payment()
    {

    }

    public Payment(int id, Date paymentDate, String paymentType, double amount, int customerId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.paymentType = paymentType;
        this.amount = amount;
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        if(amount<0)
        {
            throw new PaymentException("Amount needs to be > 0");
        }
        else
        {
            this.amount = amount;
        }
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }


    @Override
    public String toString()
    {
        return "Customer ID " + customerId + " has paid a total of $" + amount + ", payment by "
                + paymentType + " on " + paymentDate;
    }
}
