package com.JavaProject1.dao;

import com.JavaProject1.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public class PaymentDao implements IPaymentDao
{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("Id").is(id));
        Payment payment = mongoTemplate.findOne(query, Payment.class);
        return payment;
    }


    @Override
    public void save(Payment payment)
    {
        mongoTemplate.insert(payment);
    }

    @Override
    public Collection<Payment> findByType(String paymentType) {
        Query query = new Query();query.addCriteria(Criteria.where("paymentType"));
        List<Payment> pay = mongoTemplate.find(query,Payment.class);
        return pay;
    }

    @Override
    public Long rowCount() {
        Query query = new Query();
        Long result =mongoTemplate.count(query, Payment.class);
        return result;
    }

    @Override
    public Collection<Payment> getAll() {
        return mongoTemplate.findAll(Payment.class);
    }
}
